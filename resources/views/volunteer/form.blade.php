<div class="form-group row">
    <label for="name" class="col-sm-4 col-form-label">Nome</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="name" value="{{ $object ? $object->name : '' }}">
    </div>
</div>
<div class="form-group row">
    <label for="surname" class="col-sm-4 col-form-label">Cognome</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="surname" value="{{ $object ? $object->surname : '' }}">
    </div>
</div>
<div class="form-group row">
    <label for="email" class="col-sm-4 col-form-label">E-Mail</label>
    <div class="col-sm-8">
        <input type="email" class="form-control" name="email" value="{{ $object ? $object->email : '' }}">
    </div>
</div>

@if(App\Section::count() != 0)
    @if(!isset($section_id))
        <div class="form-group row">
            <label for="section_id" class="col-sm-4 col-form-label">Sezione Locale</label>
            <div class="col-sm-8">
                @include('section.select', ['select' => $object ? $object->section_id : 0, 'name' => 'section_id'])
            </div>
        </div>
    @else
        <input type="hidden" name="section_id" value="{{ $section_id }}">
    @endif
@endif
