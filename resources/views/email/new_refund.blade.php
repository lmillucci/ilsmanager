<p>
    Nuova richiesta di rimborso spese:
</p>
<ul>
    <li>{{ $refund->amount }} €</li>
    <li>{{ $refund->user ? $refund->user->printable_name : 'Utente Ignoto' }}</li>
    <li>{{ $refund->section ? $refund->section->city : 'Nessuna Sezione Locale' }}</li>
    <li>{{ $refund->notes }}</li>
    <li>{{ $refund->attachments->count() }} files allegati</li>
</ul>
<p>
    <a href="{{ route('refund.edit', $refund->id) }}">Vedi qui i dettagli.</a>
</p>
