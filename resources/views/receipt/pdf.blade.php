<html>
    <head>
        <style>

        .header {
            font-size: 0.7em;
            height: 120px;
        }

        .header img {
            float: left;
            width: 100px;
            height: 100px;
        }

        </style>
    </head>
    <body>
        <div class="header">
            <img src="{{ public_path('images/logo.png') }}">
            <p>
                <strong>{{ App\Config::getConfig('association_name') }}</strong><br>
                {!! nl2br(App\Config::getConfig('association_address')) !!}
            </p>
        </div>

        <h2>Ricevuta {{ $object->number }} del {{ $object->date }}</h2>

        <p>
            È stata ricevuta la somma di <strong>euro {{ $object->movement->amount }}</strong>, con causale:<br><br>
            {!! nl2br($object->causal) !!}
        </p>

        <br>

        @if(!empty($object->stamp))
            <p>
                Imposta di bollo assolta sull'originale<br>
                ID {{ $object->stamp }}
            </p>
        @endif

        <br>
        <br>
        <br>
        <br>

        <p>
            <i>Documento non fiscale, a quietanza del pagamento.</i>
        </p>
    </body>
</html>
