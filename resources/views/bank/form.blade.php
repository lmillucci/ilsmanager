<div class="form-group row">
    <label for="name" class="col-sm-4 col-form-label">Nome</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="name" value="{{ $object ? $object->name : '' }}">
    </div>
</div>
<div class="form-group row">
    <label for="identifier" class="col-sm-4 col-form-label">Identificativo</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="identifier" value="{{ $object ? $object->identifier : '' }}">
    </div>
</div>
<fieldset class="form-group">
    <div class="row">
        <legend class="col-form-label col-sm-4 pt-0">Tipo</legend>
        <div class="col-sm-8">
            @foreach(App\Bank::types() as $type)
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="type" value="{{ $type->identifier }}" {{ $object && $object->type == $type->identifier ? 'checked' : '' }}>
                    <label class="form-check-label" for="{{ $type->identifier }}">
                        {{ $type->label }}
                    </label>
                </div>
            @endforeach
        </div>
    </div>
</fieldset>
