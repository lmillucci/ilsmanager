$(document).ready(function() {
    $('.show-all').click(function() {
        var target = $(this).attr('data-target');
        $(target).find('.hidden').toggle();
    });

    $('.add-row').click(function(e) {
        e.preventDefault();
        var row = $(this).siblings('.hidden').find('li').clone();
        $(this).closest('.list-group-item').before(row);
    });

    $('.delete-row').click(function(e) {
        e.preventDefault();
        $(this).closest('.list-group-item').remove();
    });

    $('.multiple-files').on('change', 'input:file', function() {
        $(this).after('<input type="file" name="file[]" autocomplete="off">');
    });

    $("input.date").inputmask({
        'mask': '9999-99-99',
        'placeholder': 'yyyy-mm-dd',
        'clearIncomplete': true,
    });

    $('body').on('change', '.max-selections input:checkbox', function(e) {
        e.preventDefault();
        var container = $(this).closest('.max-selections');
        var max = parseInt(container.attr('data-max-selections'));
        var choosen = container.find('input:checkbox').filter(':checked').length;

        if (choosen >= max) {
            container.find('input:checkbox').filter(':not(:checked)').prop('disabled', true);
        }
        else {
            container.find('input:checkbox').prop('disabled', false);
        }
    });

    if ($.fn.DataTable) {
        jQuery('.table').DataTable({
            paging: false,
        });
    }
});
