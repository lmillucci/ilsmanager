<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FirstFee extends Mailable
{
    use Queueable, SerializesModels;

    private $user = null;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function build()
    {
        // gender neutral message
        return $this->subject('Ti diamo il benvenuto!')->view('email.first_fee', ['user' => $this->user]);
    }
}
